# FutureOn exercise

visual representation at https://observablehq.com/@antouank/futureon-exercise

all the code is in the file `main.js`

### how to run

- run `npm i` ( we just need `node-fetch` )
- run `node main.js` for the interactive version ( you'll be prompted to enter a staged asset id )
- run `node main.js --id THE_STAGED_ASSET_ID` for the CLI version

### code breakdown

( the code is all js to be able to run it on observable )

Just 2 main functions really.

- `retrieveRemoteData` that will hit the API and get back the staged assets and connections as dictionaries.

- `getPathsFrom` that takes an id of a staged asset and returns all the paths that start from it. To achieve that, it creates a `pathString` recursive function that runs the graph. ( cyclical graphs won't work )
