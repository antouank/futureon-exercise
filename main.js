//  imports
//
// -----------------------------------------------------------------------------
const fetch = require("node-fetch");
const readline = require("readline");

//  retrieves all the main data we need ( staged assets and connections )
//  returns a { connectionsDict, stagedAssetsDict } object
//
// -----------------------------------------------------------------------------
async function retrieveRemoteData() {
  //  constants
  //
  // ---------------------------------------------------------------------------
  const ENDPOINT = "https://backend.qa.fieldap.com";
  const PROJECT_ID = "-MA1551S-odms4rVbJ8A";
  const SUB_PROJECT_ID = "-MA1551S-odms4rVbJ8B";

  const headers = {
    "Content-Type": "application/json",
    token: "10941392-a080-44c4-8cf2-a97011bc6b81",
  };

  //  urls
  //
  // ---------------------------------------------------------------------------
  const urlStagedAssets = `${ENDPOINT}/API/v1.8/${PROJECT_ID}/subProject/${SUB_PROJECT_ID}/stagedAssets`;
  const urlConnections = `${ENDPOINT}/API/v1.8/${PROJECT_ID}/subProject/${SUB_PROJECT_ID}/connections`;
  const urlForStagedAsset = (id) =>
    `${ENDPOINT}/API/v1.8/${PROJECT_ID}/subProject/${SUB_PROJECT_ID}/stagedAsset/${id}`;
  const urlForConnection = (id) =>
    `${ENDPOINT}/API/v1.8/${PROJECT_ID}/subProject/${SUB_PROJECT_ID}/connection/${id}`;

  // get all staged assets
  const stagedAssetsDict = await fetch(urlStagedAssets, {
    headers,
  }).then((res) => res.json());

  // get all connections
  const connectionsDict = await fetch(urlConnections, {
    headers,
  }).then((res) => res.json());

  return { connectionsDict, stagedAssetsDict };
}

async function main() {
  //  initialise data
  //
  // ---------------------------------------------------------------------------
  console.log("retrieving data...");
  const { connectionsDict, stagedAssetsDict } = await retrieveRemoteData();
  console.log("ready.");

  // function to retrieve paths from a given starting staged asset
  //
  // ---------------------------------------------------------------------------
  async function getPathsFrom(stagedAssetId) {
    if (!stagedAssetId.startsWith("-")) {
      stagedAssetId = "-" + stagedAssetId;
    }

    // convert the dictionary to an array
    const connectionsList = Object.values(connectionsDict);

    // recursive function
    // takes a list of paths ( strings ), and the current asset id ( string )
    // it will check all the next assets and recusrsively extend the paths
    const pathString = (paths, currentStagedAssetId) => {
      if (stagedAssetsDict[currentStagedAssetId] === undefined) {
        throw new Error(
          "cannot find a staged asset with id : " + currentStagedAssetId
        );
      }

      // add current staged asset
      const currentPathLabel = currentStagedAssetId;

      if (paths.length === 0) {
        // first node, create path
        paths = ["|--> " + currentPathLabel];
      } else {
        // extend existing paths
        paths = paths.map((path) => path + " -> " + currentPathLabel);
      }

      //   find connections that start from this staged asset
      const connectionsFromThisAsset = connectionsList.filter(
        (connection) => connection.from.id === currentStagedAssetId
      );

      if (connectionsFromThisAsset.length === 0) {
        // no more connections, end the paths
        return paths.map((path) => path + " --|");
      } else {
        // recursively run for every path
        return paths
          .map((path) =>
            connectionsFromThisAsset
              .map((connection) => pathString([path], connection.to.id))
              .flat()
          )
          .flat();
      }
    };

    return pathString([], stagedAssetId);
  }

  //
  //
  // check for cli arg
  if (process.argv.includes("--id")) {
    let id = process.argv[process.argv.indexOf("--id") + 1];

    if (id !== undefined) {
      // argument flag approach
      //
      // -----------------------------------------------------------------------
      console.log("| Paths starting from", id);

      const paths = await getPathsFrom(id).catch((err) => {
        console.error(err);
        process.exit(1);
      });

      console.log(paths.join("\n"));
      return;
    }
  } else {
    // interactive approach
    //
    // -------------------------------------------------------------------------
    console.log("| Available staged assets : ");
    console.log(Object.keys(stagedAssetsDict).join("\n"));

    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });

    rl.question(
      `| Input your starting asset (e.g. ${Object.keys(stagedAssetsDict)[0]}):`,
      async (id) => {
        console.log("| Paths starting from", id);
        const paths = await getPathsFrom(id).catch((err) => {
          console.error(err);
          rl.close();
        });
        console.log(paths.join("\n"));

        rl.close();
      }
    );
  }

  return;
}

main();
